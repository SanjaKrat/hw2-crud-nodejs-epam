const express = require('express');
const router = new express.Router();
const authController = require('../controllers/auth.controller');

router.post('/api/auth/register', authController.register);
router.post('/api/auth/login', authController.login);

module.exports = router;
