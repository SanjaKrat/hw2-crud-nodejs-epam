const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: {
    type: String,
    trim: true,
    required: true,
  },
  password: {
    type: String,
    trim: true,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
},
{
  versionKey: false,
});

module.exports = mongoose.model('User', UserSchema);
