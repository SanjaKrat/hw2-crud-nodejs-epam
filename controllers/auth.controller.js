const userModel = require('../models/users.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const JWT_SECRET = process.env.JWT_SECRET;
const saltRounds = +process.env.BCRYPT_SALT;

module.exports = {
  register: async (req, res) => {
    try {
      const username = req.body.username;
      const password = req.body.password;
      if (!username) {
        res.status(400).send({message: `Field "username" is required`});
        return;
      }
      if (!password) {
        res.status(400).send({message: `Field "password" is required`});
        return;
      }
      const existedUser = await userModel.findOne({username});
      if (existedUser) {
        res.status(400).send({
          message: `User with username "${
            username
          }" alredy exist. Choose another username or go to login`,
        });
        return;
      }
      const encryptedPassword = await bcrypt.hash(password, saltRounds);
      await userModel.create({
        username,
        password: encryptedPassword,
      });
      res.status(200).send({message: 'Success'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  login: async (req, res) => {
    try {
      const username = req.body.username;
      const password = req.body.password;
      if (!username) {
        res.status(400).send({message: `Field "username" is required`});
        return;
      }
      if (!password) {
        res.status(400).send({message: `Field "password" is required`});
        return;
      }
      const user = await userModel.findOne({username});
      if (!user) {
        res.status(400).send({message: 'User not found'});
        return;
      }
      const isPasswordsMatch = await bcrypt.compare(password, user.password);
      if (!isPasswordsMatch) {
        res.status(400).send({message: 'Wrong password'});
        return;
      }
      const token = jwt.sign({userId: user._id}, JWT_SECRET, {expiresIn: '2h'});
      res.status(200).send({message: 'Success', jwt_token: token});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
};
