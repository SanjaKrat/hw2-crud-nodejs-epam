const jwt = require('jsonwebtoken');

const JWT_SECRET = process.env.JWT_SECRET;

const authToken = (req, res, next) => {
  let token = '';
  if (req.headers.authorization &&
    req.headers.authorization.startsWith('JWT ') ||
    req.headers.authorization.startsWith('Bearer ')) {
    token = req.headers.authorization.split(' ')[1];
  } else {
    token = req.headers.authorization;
  }
  if (!token) {
    res.status(400).send({message: 'JWT token is requred for authentication'});
    return;
  }
  try {
    const decoded = jwt.verify(token, JWT_SECRET);
    req.userId = decoded.userId;
  } catch (error) {
    res.status(400).send({message: 'Invalid JWT token'});
  }
  return next();
};

module.exports = authToken;
