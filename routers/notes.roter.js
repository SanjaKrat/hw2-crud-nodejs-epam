const express = require('express');
const router = new express.Router();
const notesController = require('../controllers/notes.controller');

const apiPoint = '/api/notes';

// get users note
router.get(apiPoint, notesController.getUsersNotes);
// add note for user
router.post(apiPoint, notesController.addNote);
// get users note by id
router.get(`${apiPoint}/:id`, notesController.getNoteById);
// update users note by id
router.put(`${apiPoint}/:id`, notesController.updateNoteById);
// check / uncheck users note by id
router.patch(`${apiPoint}/:id`, notesController.completeToggler);
// delete users note by id
router.delete(`${apiPoint}/:id`, notesController.deleteNoteById);

module.exports = router;
