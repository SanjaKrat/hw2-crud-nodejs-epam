require('dotenv').config();
require('./database/database').connect();
const express = require('express');
const logger = require('morgan');
const auth = require('./middleware/auth.middleware');
const cors = require('cors');
const authRoute = require('./routers/auth.roter');
const usersRoute = require('./routers/users.roter');
const notesRoute = require('./routers/notes.roter');

const app = express();
const PORT = process.env.PORT;

app.use(express.json());
app.use(cors());
app.use(logger('combined'));

app.use(authRoute);
app.use(auth, usersRoute);
app.use(auth, notesRoute);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
