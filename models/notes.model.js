const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const NotesSchema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
},
{
  versionKey: false,
});

module.exports = mongoose.model('Notes', NotesSchema);
