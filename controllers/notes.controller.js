const notesModel = require('../models/notes.model');

module.exports ={
  addNote: async (req, res) => {
    try {
      const userId = req.userId;
      const text = req.body.text;
      if (!userId) {
        res.status(400).send({message: 'User not found'});
        return;
      }
      if (!text) {
        res.status(400).send({message: 'Specify text field'});
        return;
      }
      await notesModel.create({userId, text});
      res.status(200).send({message: 'Success'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  getUsersNotes: async (req, res) => {
    try {
      const userId = req.userId;
      const {offset = 0, limit = 0} = req.query;
      if (!userId) {
        res.status(400).send({message: 'User not found'});
        return;
      }
      const notes = await notesModel.find({userId}).skip(offset).limit(limit);
      const count = await notesModel.find({userId}).count();
      res.status(200).send({
        offset: +offset,
        limit: +limit,
        count,
        notes,
      });
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  getNoteById: async (req, res) => {
    try {
      const userId = req.userId;
      const noteId = req.params['id'];
      console.log(noteId);
      if (!noteId) {
        res.status(400).send({message: 'No id parameter'});
        return;
      }
      const note = await notesModel.findOne({_id: noteId, userId});
      console.log(note);
      if (!note) {
        res.status(400).send({message: 'Note not found'});
        return;
      }
      res.status(200).send({note});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  updateNoteById: async (req, res) => {
    try {
      const noteId = req.params['id'];
      const userId = req.userId;
      const text = req.body.text;
      if (!noteId) {
        res.status(400).send({message: 'No id parameter'});
        return;
      }
      if (!text) {
        res.status(400).send({message: 'Specify text parameter'});
        return;
      }
      const note = await notesModel.findOne({_id: noteId, userId});
      if (!note) {
        res.status(400).send({message: 'Note not found'});
        return;
      }
      await notesModel.updateOne({_id: noteId, userId}, {text});
      res.status(200).send({message: 'Success'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  completeToggler: async (req, res) => {
    try {
      const userId = req.userId;
      const noteId = req.params['id'];
      if (!noteId) {
        res.status(400).send({message: 'No id parameter'});
        return;
      }
      const note = await notesModel.findOne({_id: noteId, userId});
      if (!note) {
        res.status(400).send({message: 'Note not found'});
        return;
      }
      const result = await notesModel.updateOne({
        _id: noteId,
        userId,
      },
      {
        completed: !note.completed,
      });
      if (!result.acknowledged) {
        res.status(500).send({message: 'Server error'});
        return;
      }
      res.status(200).send({message: 'Success'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  deleteNoteById: async (req, res) => {
    try {
      const userId = req.userId;
      const noteId = req.params['id'];
      if (!noteId) {
        res.status(400).send({message: 'No id parameter'});
        return;
      }
      const note = await notesModel.findOne({_id: noteId, userId});
      if (!note) {
        res.status(400).send({message: 'Note not found'});
        return;
      }
      const result = await notesModel.deleteOne({_id: noteId, userId});
      if (!result.acknowledged) {
        res.status(500).send({message: 'Server error'});
        return;
      }
      res.status(200).send({message: 'Success'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
};
