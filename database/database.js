const mongoose = require('mongoose');
const MONGODB_URL = process.env.MONGODB_URL;

exports.connect = () => {
  mongoose
      .connect(MONGODB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      })
      .then(() => {
        console.log('Successfully connected to database');
      })
      .catch((err) => {
        console.log('Database connection failed');
        console.error(err);
        process.exit(1);
      });
};
