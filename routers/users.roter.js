const express = require('express');
const router = new express.Router();
const userController = require('../controllers/users.controller');

const apiPoint = '/api/users/me';

// get user's profile info
router.get(apiPoint, userController.getUserInfo);
// delete user's profile
router.delete(apiPoint, userController.deleteProfile);
// Change user's password
router.patch(apiPoint, userController.changePassword);

module.exports = router;
