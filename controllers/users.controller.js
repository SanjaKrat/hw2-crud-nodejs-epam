const userModel = require('../models/users.model');
const bcrypt = require('bcrypt');

const saltRounds = +process.env.BCRYPT_SALT;

module.exports = {
  getUserInfo: async (req, res) => {
    try {
      const userId = req.userId;
      const user = await userModel.findById(userId);
      if (!user) {
        res.status(400).send({message: 'User not found'});
        return;
      }
      const {_id, username, createdDate} = user;
      res.status(200).send({user: {_id, username, createdDate}});
    } catch (error) {
      res.status(500).send('Server error');
    }
  },
  deleteProfile: async (req, res) => {
    try {
      const userId = req.userId;
      const user = await userModel.findById(userId);
      if (!user) {
        res.status(400).send({message: 'User not found'});
        return;
      }
      await userModel.deleteOne(user);
      res.status(200).send({message: 'Success'});
    } catch (error) {
      res.status(500).send('Server error');
    }
  },
  changePassword: async (req, res) => {
    try {
      const userId = req.userId;
      const oldPassword = req.body.oldPassword;
      const newPassword = req.body.newPassword;
      if (!oldPassword) {
        res.status(400).send({message: 'Specify old password'});
        return;
      }
      if (!newPassword) {
        res.status(400).send({message: 'Specify new password'});
        return;
      }
      const user = await userModel.findById(userId);
      if (!user) {
        res.status(400).send({message: 'User not found'});
        return;
      }
      const isPasswordsMatch = await bcrypt.compare(oldPassword, user.password);
      if (!isPasswordsMatch) {
        res.status(400).send({message: 'Wrong password'});
        return;
      }
      const encryptedNewPassword = await bcrypt.hash(newPassword, saltRounds);
      await userModel.updateOne(user, {password: encryptedNewPassword});
      res.status(200).send({message: 'Success'});
    } catch (error) {
      res.status(500).send('Server error');
    }
  },
};
